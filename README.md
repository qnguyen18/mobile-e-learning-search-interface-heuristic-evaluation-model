# Mobile E-learning search interface heuristic evaluation model

Heuristic evaluation is one of the most popular methods for usability
evaluation in Human-Computer Interaction (HCI). However,
most heuristic models only provide generic heuristic evaluation for
the entire application as a whole, even though different parts of
an application might serve users in different contexts, leading HCI
practitioners to miss out on context-dependent usability issues. As
a prime example, mobile search interfaces in e-learning applications
have not received a lot of attention when it comes to usability
evaluation. This research project addresses this problem by proposing a more
domain-specific and context-dependent heuristic evaluation model
for search interfaces in mobile e-learning applications. I will analyze
studies on mobile evaluation heuristics, in combination with
research in mobile search computing and e-learning heuristics, to
develop a heuristic model for mobile e-learning search interfaces.

**Software video:** https://youtu.be/pgktozRgEls

![Image of Software Diagram](/Finished-capstone-diagram.png)

                                      This is the complete version of the new heuristic evaluation model


